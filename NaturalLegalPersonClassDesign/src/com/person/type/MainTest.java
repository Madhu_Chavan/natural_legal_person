package com.person.type;
import java.text.ParseException;
import java.util.Date;

import com.person.type.Address;
import com.person.type.NaturalPerson;

public class MainTest {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws CloneNotSupportedException,ParseException {
		Address addressPune = new Address("Bharati Vidyapeeth", "Pune", "411047");
		Address addressMumbai = new Address("Grant road", "Mumbai", "400007");
		NaturalPerson []naturalPersons = new NaturalPerson[10];
		naturalPersons[0] = new NaturalPerson(101, "Thomas Ken",addressPune,new Date(90000));
		naturalPersons[1] = new NaturalPerson(102, "Emma Swan",addressMumbai, new Date(1000000000));
		
		naturalPersons[0].printPersonDetail();
		naturalPersons[1].printPersonDetail();
		
		//Though address is changed,it wont get reflected in previously created record.
		addressMumbai.setCity("New Mumbai");
		
		//Add new record having modified address
		naturalPersons[2] = new NaturalPerson(103, "Henry Archer",addressMumbai, new Date(1810000));
		
		System.out.println("-------- Modified Details -------");
		naturalPersons[0].printPersonDetail();
		naturalPersons[1].printPersonDetail();
		naturalPersons[2].printPersonDetail();
	}
}
