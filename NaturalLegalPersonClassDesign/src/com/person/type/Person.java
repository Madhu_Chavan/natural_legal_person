package com.person.type;

public abstract class Person {
	protected Integer id;
	protected String name;
	protected Address address;

	public Person() {
		// set default values
	}

	public Person(Integer id, String name, Address address) throws CloneNotSupportedException {
		super();
		this.id = id;
		this.name = name;
		this.address = (Address) address.clone();
	}

	public abstract void printPersonDetail();

}

