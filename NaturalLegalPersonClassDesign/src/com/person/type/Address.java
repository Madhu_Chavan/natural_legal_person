package com.person.type;

public class Address implements Cloneable {
	String addressLine;
	String city;
	String zip;
	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Address() {
		super();
	}

	public Address(String addressLine, String city, String zip) {
		super();
		this.addressLine = addressLine;
		this.city = city;
		this.zip = zip;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
