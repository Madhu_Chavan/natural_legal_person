package com.person.type;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NaturalPerson extends Person {

	private Date dateOfBirth;
	private Integer age;

	public NaturalPerson() {
		super();
	}

	public NaturalPerson(Integer id, String name, Address address, Date dateOfBirth) throws CloneNotSupportedException,ParseException {
		super(id, name, address);
		this.dateOfBirth = dateOfBirth;
		// derive age from DOB
		this.age = this.getAge();
	}

	@Override
	public void printPersonDetail() {
		DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		System.out.println("Id :" + this.id);
		System.out.println("Name :" + this.name);
		System.out.println("Address :" + this.address.addressLine +'|'+ this.address.city + '|'+this.address.zip);
		System.out.println("Date of birth :" + formatter.format(this.dateOfBirth));
		System.out.println("Age :" + this.age + "\n");
	}

	private Integer getAge() throws ParseException{
		Calendar today = Calendar.getInstance();
		Calendar dateOfBirth = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		dateOfBirth.setTime(formatter.parse(formatter.format(this.dateOfBirth)));
		
		int curYear = today.get(Calendar.YEAR);
		int dobYear = dateOfBirth.get(Calendar.YEAR);
		int age = curYear - dobYear;
		return age;
	}

}
