package com.person.type;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class LegalPerson extends Person {
	private Integer numberOfEmployees;
	public LegalPerson() {
		super();
	}

	public LegalPerson(Integer id, String name, Address address,Integer numberOfEmployees) throws CloneNotSupportedException {
		super(id, name, address);
		this.numberOfEmployees = numberOfEmployees;
	}

	@Override
	public void printPersonDetail() {
		System.out.println("Id :" +this.id);
		System.out.println("Name :" +this.name);
		System.out.println("Address :" +this.address);
		System.out.println("Number of employees :" +this.numberOfEmployees);
	}
}
